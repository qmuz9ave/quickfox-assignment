# QUICK_FOX_TASK
# PYTHON 3
string = "(()){{}}}}}"
valid_strings = ['(',")","{","}"]
count_dict = {}
for s in string:
  if s in valid_strings:
        count = string.count(s)
        count_dict[s] = {
          'count':count
        }
 #small parenthesis
try:
  first_sp_count = count_dict.get("(")['count'] 
except:
  first_sp_count = 0
try:
  second_sp_count = count_dict.get(")")['count']
except:
  second_sp_count = 0
small_parenthesis_difference = abs(first_sp_count - second_sp_count)  
# curly
try:
  first_p_count = count_dict.get("{")['count'] 
except:
  first_p_count = 0
try:
  second_p_count = count_dict.get("}")['count']
except:
  second_p_count = 0
parenthesis_difference = abs(first_p_count - second_p_count)
    
print("Requires: ",small_parenthesis_difference + parenthesis_difference,'Insertion')